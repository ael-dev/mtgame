# The game

![A screenshot of the game, showing the player character and a *Legman* enemy.](/screenshot.png)

This repository contains the in-development code for a high-speed, side-scrolling 2D platformer inspired by games such as *Mega Man X* and *Super Meat Boy*. The prototype is mostly finished but may receive further changes before the end of June.

For information regarding the licenses of code and other assets used in this project, see [LICENSE.md](/LICENSE.md). This code is not yet available under an open-source license because I'm having difficulty deciding which license to choose.
