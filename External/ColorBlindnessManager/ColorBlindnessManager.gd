# MIT License
#
# Copyright (c) 2020- Axel L.
# Copyright (c) 2018 Paul Joannon
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

extends Node

enum Type { NONE, PROTANOPIA, DEUTERANOPIA, TRITANOPIA, ACHROMATOPSIA, _SIZE }

onready var _colorRect = $ColorRect

func _input(event: InputEvent):
	if event is InputEventKey and event.pressed:
		match event.scancode:
			KEY_1: _colorRect.visible = false
			KEY_2: setType(Type.PROTANOPIA)
			KEY_3: setType(Type.DEUTERANOPIA)
			KEY_4: setType(Type.TRITANOPIA)
			KEY_5: setType(Type.ACHROMATOPSIA)

func setType(type: int):
	_colorRect.visible = true
	_colorRect.material.set_shader_param("type", type)
