# © 2020 Axel L. <ael dot pm at protonmail dot com>

extends Node

# Emitted when 'sendReloadSignal' is called.
signal reloadConfig

const LAYER_BIT_GROUND   = 0b00000000000000000001
const LAYER_BIT_DAMAGING = 0b00000000000000000010
const LAYER_BIT_PLAYER   = 0b10000000000000000000

var _file: ConfigFile

# Settings: Camera
var fovMultiplier: float
var isShakeEnabled: bool
var smoothingSpeed: float

# Settings: Controls
var _bindingsDict: Dictionary # Keep a local copy to prevent having to create a new one for saving
var shouldToggleSlowdown: bool

# Settings: General
var areParticleEffectsDisabled: bool
var isCRTEffectEnabled: bool
var pseudo3DFactor: float
var shouldShowFPS: bool
var slowdownMultiplier: float
var timeScale: float

func _ready(): # Override
	Debugger.verboseLog("AutoLoad: ConfigManager")
	_load()

func bindKey(actionID: int, keyCode: int, keyName: String):
	_bindingsDict[InputManager.ACTION_NAMES[actionID]] = ["k", keyName]
	InputManager.bindKey(actionID, keyCode)

func bindControllerButton(actionID: int, buttonID: int):
	_bindingsDict[InputManager.ACTION_NAMES[actionID]] = ["c", String(buttonID)]
	InputManager.bindKey(actionID, buttonID)

func sendReloadSignal():
	emit_signal("reloadConfig")

func _getBool(name: String, defaultValue: bool):
	var value = _file.get_value("Settings", name)
	return value if value is bool else defaultValue

func _getFloat(name: String, defaultValue: float):
	var value = _file.get_value("Settings", name)
	return value if value is float else defaultValue

func _getInt(name: String, defaultValue: int):
	var value = _file.get_value("Settings", name)
	return value if value is int else defaultValue

func _load():
	_file = ConfigFile.new()
	Utils.trueOrError(_file.load("res://settings.cfg") == OK)

	# Audio
	MusicManager.setMasterVolume(_getFloat("musVol", 1.0))
	MusicManager.setVolume(MusicManager.TITLE_SCREEN, _getFloat("titleScreenVol", 1.0))
	MusicManager.setVolume(MusicManager.LEVEL, _getFloat("levelVol", 1.0))
	MusicManager.setLevelMusicChoice(_getInt("levelMusicChoice", 0))

	SFManager.setMasterVolume(_getFloat("sfxVol", 1.0))
	SFManager.setVolume(SFManager.JUMP, _getBool("jumpVol", 1.0))
	SFManager.setVolume(SFManager.DASH, _getBool("dashVol", 1.0))

	# Camera
	fovMultiplier = _getFloat("fovMul", 1.0)
	isShakeEnabled = _getBool("shake", true)
	smoothingSpeed = _getFloat("smoothSpeed", 10.0)

	# Controls
	_bindingsDict = _file.get_value("Settings", "bindings")
	InputManager.setAllBindings(_bindingsDict)
	shouldToggleSlowdown = _getBool("toggleSlowdown", true)

	# General
	areParticleEffectsDisabled = _getBool("particleFxOff", false)
	isCRTEffectEnabled = _getBool("crt", false)
	pseudo3DFactor = _getFloat("p3df", 0.015)
	shouldShowFPS = _getBool("fps", false)
	slowdownMultiplier = _getFloat("slowdownMul", 0.5)
	timeScale = _getFloat("timeScale", 1.0)
