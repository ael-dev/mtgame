# © 2020 Axel L. <ael dot pm at protonmail dot com>

# A (likely inefficient) solution to reloading a level which involves
# backing up the initial state of each resettable node.

extends Node

class ResettableNode:
	var _activeNode: Node
	var _template: Node

	func _init(activeNode: Node):
		_activeNode = activeNode
		_template = _duplicate(_activeNode)

	func reset():
		var parent = _activeNode.get_parent()
		parent.remove_child(_activeNode)
		_activeNode.name = "_" + _activeNode.name
		_activeNode.call_deferred("queue_free")
		_activeNode = _duplicate(_template)
		parent.add_child(_activeNode, true)

	func setActiveNode(newActiveNode: Node):
		_activeNode = newActiveNode

	func _duplicate(node: Node):
		return node.duplicate(Node.DUPLICATE_SIGNALS | Node.DUPLICATE_GROUPS | Node.DUPLICATE_SCRIPTS)

var _player = null
var _nodesToReset = {}

func resetLevel():
	resetPlayer()
	for node in _nodesToReset.values():
		node.reset()
	SFManager.stopAll()
	ParticleManager.clear()

func resetPlayer():
	_player.reset()

# Called at the very beginning of the player's '_ready' function.
func registerPlayer(player: Node):
	assert(player != null and player.get_parent() != null)
	if _player == null:
		_player = ResettableNode.new(player)
	else:
		_player.setActiveNode(player)

# Called at the very beginning of every resettable node's '_ready' function (excl. the player's).
func registerState(node: Node):
	assert(node != null and node.get_parent() != null)
	if _nodesToReset.has(node.name):
		_nodesToReset[node.name].setActiveNode(node)
	else:
		_nodesToReset[node.name] = ResettableNode.new(node)

func clearAllExceptPlayer():
	_nodesToReset = {}
