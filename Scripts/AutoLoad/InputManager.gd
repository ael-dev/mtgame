# © 2020 Axel L. <ael dot pm at protonmail dot com>

extends Node

enum Action { LEFT, RIGHT, SLIDE, JUMP, DASH, TOGGLE_PAUSE, SLOW_TIME, TO_OVERWORLD }

class Binding:
	enum Type { KEY, CONTROLLER_BUTTON }

	var type: int
	var id: int

	func _init(bindingType: int, bindingID: int):
		type = bindingType
		id = bindingID

const ACTION_NAMES = [
	"moveLeft",
	"moveRight",
	"slide",
	"jump",
	"dash",
	"togglePause",
	"slowTime",
	"toOverworld"
]

const ACTION_NAMES_READABLE = [
	"Left",
	"Right",
	"Slide",
	"Jump",
	"Dash",
	"Pause",
	"Slow Time",
	"Overworld"
]

var _bindings = [
	Binding.new(Binding.Type.KEY, KEY_A),
	Binding.new(Binding.Type.KEY, KEY_D),
	Binding.new(Binding.Type.KEY, KEY_S),
	Binding.new(Binding.Type.KEY, KEY_L),
	Binding.new(Binding.Type.KEY, KEY_K),
	Binding.new(Binding.Type.KEY, KEY_ESCAPE),
	Binding.new(Binding.Type.KEY, KEY_SHIFT),
	Binding.new(Binding.Type.KEY, KEY_0)
]

func _ready(): # Override
	Debugger.verboseLog("AutoLoad: InputManager")

func getBindingName(actionID: int) -> String:
	assert (actionID >= 0 and actionID < _bindings.size())
	var binding = _bindings[actionID]
	match binding.type:
		Binding.Type.KEY:
			return OS.get_scancode_string(binding.id)
		Binding.Type.CONTROLLER_BUTTON:
			return "B#" + String(binding.id) # TODO Temporary
		_: # Error
			return "?"

func setAllBindings(bindingsDict: Dictionary):
	if bindingsDict == null:
		Debugger.verboseLog("Using default binding for all actions")
		for i in range(0, _bindings.size()):
			bindKey(i, _bindings[i].id)
		return

	for i in range(0, _bindings.size()):
		var name = ACTION_NAMES[i]

		var binding = bindingsDict.get(name)
		if binding is Array and binding.size() == 2 and binding[0] is String:
			match binding[0]:
				"k":
					if binding[1] is String:
						bindKey(i, OS.find_scancode_from_string(binding[1]))
				"c":
					if binding[1] is float:
						# (Controller support has not been tested)
						bindControllerButton(i, int(binding[1]))
				_:
					Debugger.verboseLog("Invalid input type '" + binding[0] + "'")

		else:
			Debugger.verboseLog("Using default binding for action '" + name + "'")
			bindKey(i, _bindings[i].id)

func bindControllerButton(index: int, buttonIndex: int):
	var name = ACTION_NAMES[index]
	Debugger.verboseLog("Bound action '" + name + "' to button #" + String(buttonIndex))
	var event = InputEventJoypadButton.new()
	event.button_index = buttonIndex
	InputMap.action_erase_events(name)
	InputMap.action_add_event(name, event)
	_bindings[index].type = Binding.Type.CONTROLLER_BUTTON
	_bindings[index].id = buttonIndex

func bindKey(index: int, scancode: int):
	var name = ACTION_NAMES[index]
	Debugger.verboseLog("Bound action '" + name + "' to key " + OS.get_scancode_string(scancode))
	var event = InputEventKey.new()
	event.scancode = scancode
	InputMap.action_erase_events(name)
	InputMap.action_add_event(name, event)
	_bindings[index].type = Binding.Type.KEY
	_bindings[index].id = scancode
