# © 2020 Axel L. <ael dot pm at protonmail dot com>

extends AudioManager

enum { TITLE_SCREEN, LEVEL }

var _currentMusicPlayer = null
var _levelMusicChoiceIndex = 0
var _levelMusicPlayers: Array

func _ready(): # Override
	._initialize("MusicLibrary")
	_levelMusicPlayers = _players[LEVEL].get_children()

func getLevelMusicChoice() -> int:
	return _levelMusicChoiceIndex

func setLevelMusic(index: int):
	assert(index >= 0 and index < _levelMusicPlayers.size())
	_levelMusicChoiceIndex = index

func play(index: int):
	if _currentMusicPlayer != null:
		_currentMusicPlayer.stop()
	if index != LEVEL:
		_currentMusicPlayer = _players[index]
	else:
		_currentMusicPlayer = _levelMusicPlayers[_levelMusicChoiceIndex]
	_currentMusicPlayer.play()

func setLevelMusicChoice(index: float):
	var _levelMusicPlayer = _levelMusicPlayers[_levelMusicChoiceIndex]
	_levelMusicChoiceIndex = index
	if _currentMusicPlayer == _levelMusicPlayer:
		play(LEVEL)

func setMasterVolume(volume: float):
	_masterVol = volume
	for i in _players.size():
		if i != LEVEL:
			_players[i].volume_db = _getDecibels(_volumes[i] * _masterVol)
		else:
			for player in _levelMusicPlayers:
				player.volume_db = _getDecibels(_volumes[i] * _masterVol)

func setVolume(index: int, volume: float):
	if index != LEVEL:
		_setLocalVolume(index, volume)
	else:
		_volumes[index] = volume
		for player in _levelMusicPlayers:
			player.volume_db = _getDecibels(volume * _masterVol)

func stop():
	assert(_currentMusicPlayer != null)
	_currentMusicPlayer.stop()
