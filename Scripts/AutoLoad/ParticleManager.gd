# © 2020 Axel L. <ael dot pm at protonmail dot com>

extends CanvasLayer

const _GOAL_PARTICLES = \
	preload("res://Scenes/Particles/GoalParticles.tscn")
const _SAWBLADE_DESTRUCTION_PARTICLES = \
	preload("res://Scenes/Particles/SawbladeDestructionParticles.tscn")
const _SAWBLADE_PORTAL_PARTICLES = \
	preload("res://Scenes/Particles/SawbladePortalParticles.tscn")

var _temporaryParticles = Node.new()
var _permanentParticles = Node.new()

func _ready(): # Override
	layer = 100
	follow_viewport_enable = true
	add_child(_temporaryParticles)
	if !ConfigManager.areParticleEffectsDisabled:
		add_child(_permanentParticles)
	Utils.connectSignal(ConfigManager, "reloadConfig", self, "_onConfigReloaded")

func _onConfigReloaded(): # Called by signal
	if ConfigManager.areParticleEffectsDisabled:
		for child in _temporaryParticles.get_children():
			child.queue_free()
		remove_child(_permanentParticles)
	elif _permanentParticles.get_parent() != self: # If particles are currently disabled
		add_child(_permanentParticles)

func createGoalParticles(template: Node2D):
	_permanentParticles.add_child(_createParticle(_GOAL_PARTICLES, template, true))

func createSawbladeDestructionParticles(template: Node2D):
	_temporaryParticles.add_child(_createParticle(_SAWBLADE_DESTRUCTION_PARTICLES, template, true))

func createSawbladePortalParticles(template: Node2D):
	_permanentParticles.add_child(_createParticle(_SAWBLADE_PORTAL_PARTICLES, template, false))

func clear():
	for child in _temporaryParticles.get_children():
		child.queue_free()
	for child in _permanentParticles.get_children():
		child.queue_free()

func _createParticle(resource: Resource, template: Node2D, isOneshot: bool):
	if ConfigManager.areParticleEffectsDisabled: return
	var particle = resource.instance()
	particle.global_position = template.global_position
	particle.one_shot = isOneshot
	return particle
