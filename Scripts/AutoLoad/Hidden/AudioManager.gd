# © 2020 Axel L. <ael dot pm at protonmail dot com>

extends Node
class_name AudioManager

var _masterVol: float
var _players: Array
var _volumes: Array

func _initialize(var libraryName: String):
	pause_mode = Node.PAUSE_MODE_PROCESS # Don't stop audio on the pause menu

	var _library = load(str("res://Scenes/", libraryName, ".tscn")).instance()
	add_child(_library)
	_players = _library.get_children()
	_volumes.resize(_players.size())
	for i in _volumes.size(): _volumes[i] = 1.0

func getMasterVolume() -> float:
	return _masterVol

func getVolume(index: int) -> float:
	return _volumes[index]

func isMuted() -> bool:
	return (_masterVol == 0.0)

func _getDecibels(volume: float):
	assert(volume >= 0.0)
	if volume == 0.0: volume = 0.0001
	return (10.0 * log(volume))

func _setLocalVolume(index: int, localVolume: float):
	_volumes[index] = localVolume
	_players[index].volume_db = _getDecibels(localVolume * _masterVol)
