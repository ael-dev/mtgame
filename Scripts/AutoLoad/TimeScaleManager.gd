# © 2020 Axel L. <ael dot pm at protonmail dot com>

extends Node

var _timeScaleMultiplier = 1.0
var _timeScale = 1.0

func getTimeScaleMultiplier() -> float:
	return _timeScaleMultiplier

func setTimeScaleMultiplier(multiplier: float):
	_timeScaleMultiplier = multiplier
	_update()

func setTimeScale(timeScale: float):
	_timeScale = timeScale
	_update()

func _update():
	Engine.time_scale = _timeScale * _timeScaleMultiplier
