# © 2020 Axel L. <ael dot pm at protonmail dot com>

# A class which can be used to get references to various nodes.

extends Node

func getGameplayManager() -> Node:
	return $"/root/Root/GameplayManager"
func getLevelManager() -> Node:
	return $"/root/Root/GameplayManager/LevelManager"
func getPauseMenu() -> Node:
	return $"/root/Root/GameplayManager/PauseCanvasLayer/PauseMenu"
func getPlayer() -> Node:
	return $"/root/Root/GameplayManager/Player"
