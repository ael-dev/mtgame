# © 2020 Axel L. <ael dot pm at protonmail dot com>

# A class containing various useful functions.

extends Node

func connectSignal(sender: Object, signalName: String, receiver: Object, functionName: String):
	if sender.is_connected(signalName, receiver, functionName): return

	var error = sender.connect(signalName, receiver, functionName)
	if error != OK:
		printerr("Error code ", error, ". Signal '", signalName, "' could not be connected from ", \
			sender.name, " to ", receiver.name, " with the function name '", functionName, "'")
		print_stack()
		_printLine()

func disconnectSignal(sender: Object, signalName: String, receiver: Object, functionName: String):
	if sender.is_connected(signalName, receiver, functionName):
		sender.disconnect(signalName, receiver, functionName)

func die(message = ""):
	printerr("A fatal error occurred.")
	if !message.empty():
		printerr("Message: ", message)
	print_stack()
	_printLine()
	get_tree().quit()

func printError(message = ""):
	printerr("A non-fatal error occurred.")
	if !message.empty():
		printerr("Message: ", message)
	print_stack()
	_printLine()

func trueOrError(boolean: bool, message = ""):
	if !boolean:
		printError(message)

func _printLine():
	print("--------------------------------------------------------------------------------")
