# © 2020 Axel L. <ael dot pm at protonmail dot com>

extends AudioManager

enum { JUMP, DASH, SLIDE, PLAYER_DEATH, LEGMAN_STOMP, SAWBLADE_DESTRUCTION }

var _masterVolDb: float # Remove need for calculation in 'makePlayer2D'

func _ready(): # Override
	._initialize("SoundEffectLibrary")

func makePlayer2D(index: int) -> AudioStreamPlayer2D:
	var asp = AudioStreamPlayer2D.new()
	asp.stream = _players[index].stream
	asp.volume_db = _masterVolDb
	return asp

func play(index: int):
	_players[index].play()

func setMasterVolume(volume: float):
	_masterVol = volume
	_masterVolDb = _getDecibels(_masterVol)
	for i in _players.size():
		_players[i].volume_db = _getDecibels(_volumes[i] * _masterVol)

func setVolume(index: int, volume: float):
	_setLocalVolume(index, volume)

func stop(index: int):
	assert(index >= 0 and index < _players.size())
	_players[index].stop()

func stopAll():
	for sed in _players:
		sed.stop()

func updateVolume(asp: AudioStreamPlayer2D):
	asp.volume_db = _masterVolDb
