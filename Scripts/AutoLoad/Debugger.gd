# © 2020 Axel L. <ael dot pm at protonmail dot com>

extends Node

const IS_VERBOSE_ENABLED = false

func _ready():
	Debugger.verboseLog("AutoLoad: Debugger")

func verboseLog(message: String):
	if IS_VERBOSE_ENABLED:
		print(message)
