# © 2020 Axel L. <ael dot pm at protonmail dot com>

# A makeshift solution to vertically center a UI item relative to its parent.
# (Precision depends on how well the item's rectangle actually matches its height.)

extends Control

func _ready(): # Override
	rect_position.y = (get_parent().rect_size.y / 2) - (rect_size.y / 2)
