# © 2020 Axel L. <ael dot pm at protonmail dot com>

# Used to remap a particular input action.
# Currently only works with keyboard keys.

extends Control

signal mappingChanged

const FORMAT_STRING = "[center]"

var _actionID: int
var _isWaitingForKey = false

func _input(event: InputEvent): # Override
	if !_isWaitingForKey: return

	if event is InputEventKey:
		var keyName = OS.get_scancode_string(event.scancode)
		$BindingLabel.bbcode_text = FORMAT_STRING + keyName
		ConfigManager.bindKey(_actionID, event.scancode, keyName)
		_stopWaitingForKey()

	elif event is InputEventJoypadButton:
		$BindingLabel.bbcode_text = FORMAT_STRING + "B" + String(event.button_index)
		ConfigManager.bindControllerButton(_actionID, event.button_index)
		_stopWaitingForKey()

func _onButtonPressed(): # Called by signal
	_isWaitingForKey = true
	$Button.text = "PRESS A KEY"

func initialize(actionID: int):
	_actionID = actionID
	$ActionLabel.bbcode_text = FORMAT_STRING + "Action '" + \
		InputManager.ACTION_NAMES_READABLE[_actionID] + "'"
	$BindingLabel.bbcode_text = FORMAT_STRING + InputManager.getBindingName(actionID)
	$Button.text = "PRESS TO REMAP"
	Utils.connectSignal($Button, "pressed", self, "_onButtonPressed")

func _stopWaitingForKey():
	$Button.text = "PRESS TO REMAP"
	_isWaitingForKey = false
	emit_signal("mappingChanged")
