# © 2020 Axel L. <ael dot pm at protonmail dot com>

# Contains an array of 'Mapper' nodes, each corresponding to an input action.

extends Container

func _ready(): # Override
	var actionID = 0
	for mapper in get_children():
		mapper.initialize(actionID)
		actionID += 1
