# © 2020 Axel L. <ael dot pm at protonmail dot com>

# Handles updating ConfigManager and updating the displayed values based on ConfigManager.

extends Control

func _ready(): # Override
	$MusicMasterVolume/Value.value        = MusicManager.getMasterVolume()
	$TitleScreenMusicVolume/Value.value   = MusicManager.getVolume(MusicManager.TITLE_SCREEN)
	$LevelMusicVolume/Value.value         = MusicManager.getVolume(MusicManager.LEVEL)
	$LevelMusicChoice/Value.value         = MusicManager.getLevelMusicChoice()
	$SoundEffectMasterVolume/Value.value  = SFManager.getMasterVolume()
	$JumpSoundVolume/Value.value          = SFManager.getVolume(SFManager.JUMP)
	$DashSoundVolume/Value.value          = SFManager.getVolume(SFManager.DASH)
	$SlideSoundVolume/Value.value         = SFManager.getVolume(SFManager.SLIDE)
	$TimeScale/Value.value                = ConfigManager.timeScale
	$SlowdownMultiplier/Value.value       = ConfigManager.slowdownMultiplier
	$ToggleSlowdown/Value.pressed         = ConfigManager.shouldToggleSlowdown
	$DisableParticleEffects/Value.pressed = ConfigManager.areParticleEffectsDisabled
	$P3DF/Value.value                     = ConfigManager.pseudo3DFactor
	$CamShake/Value.pressed               = ConfigManager.isShakeEnabled
	$CamSmoothingSpeed/Value.value        = ConfigManager.smoothingSpeed
	$FOVMultiplier/Value.value            = ConfigManager.fovMultiplier
	$CRTEffect/Value.pressed              = ConfigManager.isCRTEffectEnabled
	$FPS/Value.pressed                    = ConfigManager.shouldShowFPS

	Utils.connectSignal($MusicMasterVolume/Value, "value_changed", \
		self, "_musicMasterVolumeChanged")
	Utils.connectSignal($TitleScreenMusicVolume/Value, "value_changed", \
		self, "_titleScreenMusicVolumeChanged")
	Utils.connectSignal($LevelMusicChoice/Value, "value_changed", \
		self, "_levelMusicChoiceChanged")
	Utils.connectSignal($LevelMusicVolume/Value, "value_changed", \
		self, "_levelMusicVolumeChanged")

	Utils.connectSignal($SoundEffectMasterVolume/Value, "value_changed", \
		self, "_soundEffectMasterVolumeChanged")
	Utils.connectSignal($JumpSoundVolume/Value, "value_changed", self, "_jumpSoundVolumeChanged")
	Utils.connectSignal($DashSoundVolume/Value, "value_changed", self, "_dashSoundVolumeChanged")
	Utils.connectSignal($SlideSoundVolume/Value, "value_changed", self, "_slideSoundVolumeChanged")

func updateConfig():
	ConfigManager.timeScale                  = $TimeScale/Value.value
	ConfigManager.slowdownMultiplier         = $SlowdownMultiplier/Value.value
	ConfigManager.shouldToggleSlowdown       = $ToggleSlowdown/Value.pressed
	ConfigManager.areParticleEffectsDisabled = $DisableParticleEffects/Value.pressed
	ConfigManager.pseudo3DFactor             = $P3DF/Value.value
	ConfigManager.isShakeEnabled             = $CamShake/Value.pressed
	ConfigManager.smoothingSpeed             = $CamSmoothingSpeed/Value.value
	ConfigManager.fovMultiplier              = $FOVMultiplier/Value.value
	ConfigManager.isCRTEffectEnabled         = $CRTEffect/Value.pressed
	ConfigManager.shouldShowFPS              = $FPS/Value.pressed
	ConfigManager.sendReloadSignal()
	TimeScaleManager.setTimeScale(ConfigManager.timeScale)

func _dashSoundVolumeChanged(newVolume: float): # Called by signal
	SFManager.setVolume(SFManager.DASH, newVolume)
	SFManager.play(SFManager.DASH)

func _jumpSoundVolumeChanged(newVolume: float): # Called by signal
	SFManager.setVolume(SFManager.JUMP, newVolume)
	SFManager.play(SFManager.JUMP)

func _levelMusicChoiceChanged(newChoice: int): # Called by signal
	MusicManager.setLevelMusicChoice(newChoice)

func _levelMusicVolumeChanged(newVolume: float): # Called by signal
	MusicManager.setVolume(MusicManager.LEVEL, newVolume)

func _musicMasterVolumeChanged(newVolume: float): # Called by signal
	MusicManager.setMasterVolume(newVolume)

func _slideSoundVolumeChanged(newVolume: float): # Called by signal
	SFManager.setVolume(SFManager.SLIDE, newVolume)
	SFManager.play(SFManager.SLIDE)

func _titleScreenMusicVolumeChanged(newVolume: float): # Called by signal
	MusicManager.setVolume(MusicManager.TITLE_SCREEN, newVolume)

func _soundEffectMasterVolumeChanged(newVolume: float): # Called by signal
	SFManager.setMasterVolume(newVolume)
	SFManager.play(SFManager.JUMP)
