# © 2020 Axel L. <ael dot pm at protonmail dot com>

# Sets the text of all child labels to the corresponding key/button binding.

extends Node

const _ACTION_ID_DICT = {
	"L": InputManager.Action.LEFT,
	"R": InputManager.Action.RIGHT,
	"S": InputManager.Action.SLIDE,
	"J": InputManager.Action.JUMP,
	"D": InputManager.Action.DASH,
	"P": InputManager.Action.TOGGLE_PAUSE,
	"T": InputManager.Action.SLOW_TIME,
	"O": InputManager.Action.TO_OVERWORLD
}

export(String) var formatString = ""

var _originalTexts = []

func _ready(): # Override
	_originalTexts.resize(get_child_count())
	for i in _originalTexts.size():
		_originalTexts[i] = formatString + get_child(i).bbcode_text
	_onConfigReloaded()
	Utils.connectSignal(ConfigManager, "reloadConfig", self, "_onConfigReloaded")

func _onConfigReloaded(): # Called by signal and '_ready'
	for i in get_child_count():
		get_child(i).bbcode_text = _updateLabelText(_originalTexts[i])

func _updateLabelText(text: String) -> String:
	var i = 0
	while true:
		if text[i] == "(":
			if text[i+2] != ")":
				Utils.die("Internal error: invalid formatting")
				return ""
			var stringToInsert = InputManager.getBindingName(_ACTION_ID_DICT[text[i+1]])
			text.erase(i, 3)
			text = text.insert(i, stringToInsert)
			i += stringToInsert.length()
		else:
			i += 1
		if i >= text.length(): break
	return text
