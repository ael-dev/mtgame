# © 2020 Axel L. <ael dot pm at protonmail dot com>

extends Node2D

var _prevFPS = 0.0

onready var _label: RichTextLabel = $Label

func _ready(): # Override
	_updateVisibility()
	Utils.connectSignal(ConfigManager, "reloadConfig", self, "_updateVisibility")

func _updateVisibility(): # Called by signal and '_ready'
	visible = ConfigManager.shouldShowFPS
	set_process(visible)

func _process(_dt):
	var fps = Engine.get_frames_per_second()
	if fps != _prevFPS:
		_label.bbcode_text = "[center]FPS: " + String(fps)
