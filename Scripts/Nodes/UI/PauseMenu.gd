# © 2020 Axel L. <ael dot pm at protonmail dot com>

# Handles toggling between the pause menu and gameplay.

extends Control

func _ready(): # Override
	Utils.connectSignal($ScrollContainer/Settings/MapperCollection/MapperPause, \
		"mappingChanged", self, "_escapeMappingChanged")

func _process(_dt: float): # Override
	if !visible:
		if Input.is_action_just_pressed("togglePause"):
			$Camera.current = true
			get_tree().paused = true
			visible = true

	else:
		if Input.is_action_just_pressed("togglePause"):
			visible = false
			if !$"/root/Root".has_node("TitleScreen"): # If not on the title screen
				Nodes.getPlayer().get_node("Camera").current = true
			get_tree().paused = false
			$ScrollContainer/Settings.updateConfig()

func _escapeMappingChanged(): # Called by signal
	# Update the "Press [...] to return." label.
	ConfigManager.sendReloadSignal()
