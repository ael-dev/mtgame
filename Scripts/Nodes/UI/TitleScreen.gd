# © 2020 Axel L. <ael dot pm at protonmail dot com>

extends CanvasLayer

onready var _creditsPopup = $CreditsPopup

func _ready(): # Override
	MusicManager.play(MusicManager.TITLE_SCREEN)

func _input(event: InputEvent): # Override
	if event.is_action_type():
		if event.is_action_released("jump"):
			Nodes.getGameplayManager().start()
			queue_free()
		elif event is InputEventKey and event.scancode == KEY_C and !event.pressed:
			_creditsPopup.visible = !_creditsPopup.visible
