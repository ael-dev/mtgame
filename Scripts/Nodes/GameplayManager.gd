# © 2020 Axel L. <ael dot pm at protonmail dot com>

extends Node

onready var _player = $Player

func _input(event: InputEvent): # Override
	if event.is_action_type():
		if Input.is_action_just_pressed("toOverworld"): # Next level
			$LevelManager.goToOverworld()

func _ready(): # Override
	remove_child(_player)
	set_process_input(false)

func _onLevelEntered(): # Called by signal
	$TopCanvasLayer/LoadingScreen.queue_free()
	add_child(_player)
	set_process_input(true)

func start(): # Called by the title screen
	Utils.connectSignal($LevelManager, "levelEntered", self, "_onLevelEntered")
	$LevelManager.start()
