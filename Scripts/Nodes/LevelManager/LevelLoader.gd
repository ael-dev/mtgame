# © 2020 Axel L. <ael dot pm at protonmail dot com>

extends Object

class_name LevelLoader

# Emitted when the level with number 'number' has been loaded.
signal levelLoaded(number)

var _levelCount: int
var _levelTemplates = []
var _numberOfLevelsLoaded = 0
var _thread = Thread.new()

# Changing this to '_init' causes the following error:
#   create_instance: Condition "r_error.error != Variant::CallError::CALL_OK" is true.
#   Returned: __null <C++ Source> modules/gdscript/gdscript.cpp:127 @ _create_instance()
func start(levelCount: int):
	_levelCount = levelCount
	_levelTemplates.resize(levelCount)
	_thread.start(self, "_load")

func _load(_userdata) -> Resource: # Called by thread
	var res = load("res://Scenes/Levels/Overworld.tscn") if _numberOfLevelsLoaded == 0 else \
		load(str("res://Scenes/Levels/Level", _numberOfLevelsLoaded, ".tscn"))
	call_deferred("_onLoadFinished")
	return res

func _onLoadFinished():
	Debugger.verboseLog(str("LevelLoader: loaded level ", _numberOfLevelsLoaded))
	var resource: Resource = _thread.wait_to_finish()
	_levelTemplates[_numberOfLevelsLoaded] = resource.instance()
	emit_signal("levelLoaded", _numberOfLevelsLoaded) # Must be between the above and below commands
	_numberOfLevelsLoaded += 1
	if _numberOfLevelsLoaded < _levelCount:
		_thread.start(self, "_load")

func getLevelInstance(index: int) -> Node:
	assert(hasLevelBeenLoaded(index))
	return _levelTemplates[index].duplicate( \
		Node.DUPLICATE_SIGNALS | Node.DUPLICATE_GROUPS | Node.DUPLICATE_SCRIPTS)

func hasLevelBeenLoaded(number: int) -> bool:
	return (_numberOfLevelsLoaded > number)
