# © 2020 Axel L. <ael dot pm at protonmail dot com>

# Handles transitions between levels.

extends Node

signal goalReached
signal levelEntered
signal playerKilled

const LEVEL_COUNT = 7

onready var _completionLabel = $CanvasLayer/BindingTextReplacer/Label
onready var _completionRect = $CanvasLayer/BGRect
onready var _completionTween = $CanvasLayer/Tween
var _currentLevel = null
var _currentLevelNumber = 1
var _levelLoader = LevelLoader.new()
onready var _playerDeathTimer = $PlayerDeathTimer

func _ready(): # Override
	set_process_input(false)
	set_process(false)
	_levelLoader.start(LEVEL_COUNT + 1) # number of levels + overworld

func _input(event: InputEvent): # Override
	if event.is_action_type():
		if Input.is_action_just_pressed("jump"): # Next level
			set_process(true)
			_currentLevelNumber = (_currentLevelNumber % LEVEL_COUNT) + 1
		elif Input.is_action_just_pressed("moveLeft"): # Restart level
			LevelResetter.resetLevel()
		elif Input.is_action_just_pressed("moveRight"): # Go to overworld
			set_process(true)
			_currentLevelNumber = 0
		else:
			return
		set_process_input(false)
		Nodes.getPauseMenu().set_process(true)
		for node in [ _completionRect, _completionLabel ]: node.visible = false

# Used since levels cannot be changed within a signal handler.
func _process(_dt: float): # Override
	if _currentLevel != null:
		SFManager.stopAll()
		ParticleManager.clear()
		LevelResetter.clearAllExceptPlayer()

		remove_child(_currentLevel)
		_currentLevel.queue_free()

		# Reset the player before loading the level to allow newly
		# loaded nodes to connect their signals to the player.
		LevelResetter.resetPlayer()

	_currentLevel = _levelLoader.getLevelInstance(_currentLevelNumber)
	add_child(_currentLevel)
	if _currentLevelNumber == 0:
		for portal in _currentLevel.get_node("LevelPortals").get_children():
			Utils.connectSignal(portal, "levelPortalEntered", self, "_onLevelPortalEntered")
	else:
		Utils.connectSignal(_currentLevel.get_node("Goal"), "goalReached", self, "_onGoalReached")

	set_process(false)

func _onGoalReached(): # Called by signal
	ParticleManager.clear()
	SFManager.stopAll()
	Nodes.getPauseMenu().set_process(false)
	Engine.time_scale = 0.2
	emit_signal("goalReached")
	set_process_input(true)
	for node in [ _completionRect, _completionLabel ]:
		node.visible = true
		Utils.trueOrError(_completionTween.interpolate_property(node, "modulate:a", 0.0, 1.0, 0.1))
	Utils.trueOrError(_completionTween.start())

func _onPlayerDeathTimerDone(): # Called by signal
	Utils.disconnectSignal(_playerDeathTimer, "timeout", self, "_onPlayerDeathTimerDone")
	Nodes.getPauseMenu().set_process(true)
	Engine.time_scale = 1.0
	LevelResetter.resetLevel()

func goToOverworld():
	if !is_processing_input():
		set_process(true)
		_currentLevelNumber = 0

func restart(): # Called by the player or an obstacle when the player "dies"
	Nodes.getPauseMenu().set_process(false)
	Engine.time_scale = 0.2
	_playerDeathTimer.start(1.0 * Engine.time_scale)
	Utils.connectSignal(_playerDeathTimer, "timeout", self, "_onPlayerDeathTimerDone")
	emit_signal("playerKilled")

func _onLevelLoaded(number: int): # Called by signal
	if number >= _currentLevelNumber:
		Utils.disconnectSignal(_levelLoader, "levelLoaded", self, "_onLevelLoaded")
		emit_signal("levelEntered")
		_enterFirstLevel()

func _onLevelPortalEntered(number: int): # Called by signal
	_currentLevelNumber = number
	set_process(true)

func start(): # Called by GameplayManager
	if !_levelLoader.hasLevelBeenLoaded(_currentLevelNumber):
		Utils.connectSignal(_levelLoader, "levelLoaded", self, "_onLevelLoaded")
	else:
		emit_signal("levelEntered")
		_enterFirstLevel()

func _enterFirstLevel():
	set_process(true)
	MusicManager.play(MusicManager.LEVEL)
