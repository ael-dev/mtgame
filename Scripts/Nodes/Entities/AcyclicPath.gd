# © 2020 Axel L. <ael dot pm at protonmail dot com>

extends Path2D

onready var _duration = 0.003 * curve.get_baked_length()
onready var _follower = get_child(get_child_count() - 1)
var _nextEndValue = 1
var _pathFollow = PathFollow2D.new()
var _tween = Tween.new()

func _ready(): # Override
	assert(get_child_count() == 3)
	LevelResetter.registerState(self)
	visible = false
	remove_child(_follower)

func spawn():
	visible = true
	$Line2D.points = curve.get_baked_points()

	# Add the follower node as a child to the PathFollow2D node.
	_follower.visible = true
	_pathFollow.add_child(_follower)
	add_child(_pathFollow)

	# Start the movement.
	add_child(_tween)
	_tween.connect("tween_all_completed", self, "_updateMovement")
	_updateMovement()

func _updateMovement(): # Called by signal and 'spawn'
	var startValue = (_nextEndValue + 1) % 2
	_tween.interpolate_property(_pathFollow, "unit_offset", float(startValue), \
		float(_nextEndValue), _duration, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	_nextEndValue = startValue
	_tween.start()
