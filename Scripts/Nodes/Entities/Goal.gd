# © 2020 Axel L. <ael dot pm at protonmail dot com>

extends Area2D

# Emitted when the player enters this Area.
signal goalReached

func _ready(): # Override
	Utils.connectSignal(self, "body_entered", self, "_onBodyEntered")
	ParticleManager.createGoalParticles(self)

func _onBodyEntered(_body: Node): # Called by signal
	emit_signal("goalReached")
