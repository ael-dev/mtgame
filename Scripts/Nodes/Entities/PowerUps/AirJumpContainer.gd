# © 2020 Axel L. <ael dot pm at protonmail dot com>

# Provides a power-up which gives the player the ability to air jump once before landing.

extends Area2D

onready var _collisionShape = $CollisionShape2D
var _sizeTween = Tween.new()
var _timer = Timer.new()

func _ready(): # Override
	add_child(_sizeTween)
	add_child(_timer)
	_timer.wait_time = 2.0
	LevelResetter.registerState(self)
	Utils.connectSignal(self, "body_entered", self, "_onBodyEntered")

func _onBodyEntered(node: Node): # Called by signal
	if node.name == "Player":
		node.get_node("PowerUpFSM").giveAirJump()
		_collisionShape.set_deferred("disabled", true)
		_fade(1.0, 0.0)
		Utils.connectSignal(_sizeTween, "tween_all_completed", self, "_onInvisible")

func _onInvisible(): # Called by signal
	Utils.disconnectSignal(_sizeTween, "tween_all_completed", self, "_onInvisible")
	_timer.start()
	Utils.connectSignal(_timer, "timeout", self, "_onTimeout")

func _onTimeout(): # Called by signal
	_fade(0.0, 1.0)
	_collisionShape.disabled = false

func _fade(startValue: float, endValue: float):
	_sizeTween.interpolate_property($AnimatedSprite, "scale", \
		Vector2(startValue, startValue), Vector2(endValue, endValue), 0.2)
	_sizeTween.start()
