# © 2020 Axel L. <ael dot pm at protonmail dot com>

extends Area2D

# Emitted when the player enters this Area.
# Argument: the number of the level to which the portal leads (int).
signal levelPortalEntered(number)

export var number = 0

func _ready(): # Override
	assert(number > 0)
	Utils.connectSignal(self, "body_entered", self, "_onBodyEntered")
	ParticleManager.createGoalParticles(self)
	Utils.connectSignal(ConfigManager, "reloadConfig", self, "_updateVisibility")
	_updateVisibility()

func _onBodyEntered(_body: Node): # Called by signal
	emit_signal("levelPortalEntered", number)
	SFManager.stopAll()

func _updateVisibility(): # Called by signal and in '_ready'
	$Particles.emitting = !ConfigManager.areParticleEffectsDisabled
	$Particles.visible = $Particles.emitting
