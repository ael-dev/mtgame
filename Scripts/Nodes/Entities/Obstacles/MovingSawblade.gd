# © 2020 Axel L. <ael dot pm at protonmail dot com>

# Continuously moves the attached node along a line at a fixed velocity.

extends KinematicBody2D

var _velocity: Vector2

func _physics_process(dt: float): # Override
	var collision = move_and_collide(_velocity * dt)
	if collision != null:
		if collision.collider.collision_layer & ConfigManager.LAYER_BIT_PLAYER:
			Nodes.getLevelManager().restart()
		elif collision.collider.collision_layer & ConfigManager.LAYER_BIT_GROUND:
			set_physics_process(false)
			_destroy()

func setDirection(direction: Vector2):
	_velocity = Vector2(500.0, 500.0) * direction

func _destroy():
	$CollisionShape2D.queue_free()

	if !SFManager.isMuted():
		var sfPlayer = SFManager.makePlayer2D(SFManager.SAWBLADE_DESTRUCTION)
		add_child(sfPlayer)
		sfPlayer.play()

	ParticleManager.createSawbladeDestructionParticles(self)

	var tween = Tween.new()
	add_child(tween)
	tween.interpolate_property(self, "scale", Vector2(1.0, 1.0), Vector2(0.0, 0.0), 0.3)
	tween.start()
	Utils.connectSignal(tween, "tween_all_completed", self, "queue_free")
