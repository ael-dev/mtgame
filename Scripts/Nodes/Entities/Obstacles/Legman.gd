# © 2020 Axel L. <ael dot pm at protonmail dot com>

# Represents a very basic enemy that walks in a direction and turns when it hits a wall. No gravity
# calculations are currently implemented, so the enemy can only walk along a straight line.

extends KinematicBody2D

const _SPEED_X = 200.0

enum DirectionX { LEFT = -1, RIGHT = 1 }

export(DirectionX) var startDirectionX = DirectionX.LEFT

onready var _animSprite = $AnimatedSprite
onready var _dirX = startDirectionX
var _speedX = _SPEED_X
var _stompPlayer: AudioStreamPlayer2D
onready var _wallRayCasts = $WallRayCasts

func _ready(): # Override
	LevelResetter.registerState(self)
	set_physics_process(false)
	for rc in _wallRayCasts.get_children():
		rc.add_exception(self)

func _physics_process(_dt: float): # Override
	_checkRayCasts()

	var _vec = move_and_slide(Vector2(_speedX * _dirX, 0.0))
	for i in get_slide_count():
		var collision: KinematicCollision2D = get_slide_collision(i)
		if collision.collider.collision_layer & ConfigManager.LAYER_BIT_PLAYER:
			Nodes.getLevelManager().restart()
			break

func _onTurningFinished(): # Called by signal
	Utils.disconnectSignal(_animSprite, "animation_finished", self, "_onTurningFinished")
	_speedX = _SPEED_X
	_dirX = -_dirX
	_animSprite.flip_h = !_animSprite.flip_h
	_animSprite.play("Walking")
	_animSprite.frame = 16 # Correctly position front leg

func _onAnimationFrameChanged(): # Called by signal
	match _animSprite.frame:
		3, 18:
			if !_stompPlayer.playing and !SFManager.isMuted():
				SFManager.updateVolume(_stompPlayer)
				_stompPlayer.play()
			if _animSprite.animation[0] == "W": # "Walking"
				_setSpeedScale(0.7)
		7, 22:
			if _animSprite.animation[0] == "W": # "Walking"
				_setSpeedScale(1.0)

func spawn():
	if _dirX == DirectionX.RIGHT:
		_animSprite.flip_h = true
		_flipRayCasts()

	Utils.connectSignal(_animSprite, "frame_changed", self, "_onAnimationFrameChanged")

	_stompPlayer = SFManager.makePlayer2D(SFManager.LEGMAN_STOMP)
	_stompPlayer.attenuation = 3.0
	add_child(_stompPlayer)

	set_physics_process(true)

func _checkRayCasts():
	for rc in _wallRayCasts.get_children():
		if rc.is_colliding():
			_speedX = 0.0
			_animSprite.speed_scale = 1.0
			_animSprite.play("Turning")
			Utils.connectSignal(_animSprite, "animation_finished", self, "_onTurningFinished")
			_flipRayCasts()
			return

func _flipRayCasts():
	for rc in _wallRayCasts.get_children():
		rc.cast_to.x = -rc.cast_to.x

func _setSpeedScale(speedScale: float):
	_speedX = speedScale * _SPEED_X
	_animSprite.speed_scale = speedScale
