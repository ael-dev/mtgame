# © 2020 Axel L. <ael dot pm at protonmail dot com>

extends StaticBody2D

export var secondsBetweenSpawns = 1.0
export var direction = Vector2(.0, .0)
export var shouldBeInvisible = false

var _sawbladeTemplate: Node2D
var _spawnTimer: SimpleTimer

func _ready(): # Override
	if shouldBeInvisible:
		$AnimatedSprite.queue_free()
		$CollisionShape2D.queue_free()
	LevelResetter.registerState(self)
	_spawnTimer = SimpleTimer.new(secondsBetweenSpawns)
	_sawbladeTemplate = $MovingSawblade.duplicate(DUPLICATE_SCRIPTS)
	$MovingSawblade.set_physics_process(false)
	set_physics_process(false)

func _physics_process(dt: float): # Override
	_spawnTimer.update(dt)
	if _spawnTimer.hasReachedTimeLimit():
		var sawblade = _sawbladeTemplate.duplicate(DUPLICATE_SCRIPTS)
		sawblade.setDirection(direction)
		add_child(sawblade)
		_spawnTimer.start()

func spawn():
	$MovingSawblade.set_physics_process(true)
	set_physics_process(true)
	$MovingSawblade.setDirection(direction)
	if !shouldBeInvisible:
		ParticleManager.createSawbladePortalParticles(self)
	_spawnTimer.start()
