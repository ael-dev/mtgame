# © 2020 Axel L. <ael dot pm at protonmail dot com>

# Handles the player's dashing.

extends Node

enum Status { CAN_START_AND_CONTINUE, CAN_START, CAN_CONTINUE, MUST_STOP }

onready var _animMan = $"../Visuals/AnimationManager"
onready var _camera = $"../Camera"
onready var _fi = get_parent().getFrameInfo()
onready var _player = get_parent()
onready var _trailMan = $"../TrailManager"
var _animationModifier = ""
var _dashPressTimer = SimpleTimer.new(0.1)
var _mostRecentStateID = -1

# Called by the player when a level is cleared.
func forceStop():
	if isDashing():
		_stopDashing()

func getAnimationModifier() -> String:
	return _animationModifier

func isDashing() -> bool:
	return (_animationModifier != "")

func notifyFaceDirectionFlipped():
	if isDashing() and _fi.isGrounded:
		_stopDashing()

func notifyMainStateChange():
	var newStateID = _player.getMainFSM().getCurrentStateID()

	if isDashing():
		if _dashPressTimer.hasStarted() and newStateID == Player.State.IDLE:
			# Can't be IDLE when dashing, so transition to RUNNING.
			_fi.inputDir = _player.getFaceDirection()
			_player.getMainFSM().enter(Player.State.RUNNING)

		elif newStateID == Player.State.WALL_JUMP_START_1:
			# If transitioning from FALLING to WALL_JUMP_START_1 without WALL_SLIDING between,
			# ensure that the player has pressed DASH again in order to start DASH JUMPING
			# from the wall; otherwise, they will DASH JUMP with or without meaning to.
			if _mostRecentStateID == Player.State.FALLING:
				if !_dashPressTimer.hasStarted():
					_stopDashing()

		else:
			if _getStatus() in [ Status.CAN_START, Status.MUST_STOP ]:
				_stopDashing()
			else:
				_enableModifiers()

	_mostRecentStateID = newStateID

func update():
	if _dashPressTimer.hasStarted():
		_dashPressTimer.update(_fi.dt)
		if _dashPressTimer.hasReachedTimeLimit():
			_dashPressTimer.stop()

	if _getStatus() in [ Status.CAN_START_AND_CONTINUE, Status.CAN_START ]:
		if _dashPressTimer.hasStarted():
			_dashPressTimer.stop()
			_startDashing()
		elif Input.is_action_just_pressed("dash"):
			_startDashing()
		elif Input.is_action_just_released("dash"):
			_stopDashing()

	else:
		if Input.is_action_just_pressed("dash"):
			_dashPressTimer.start()

func _enableModifiers():
	_animationModifier = "+D"
	_animMan.setDashingColorEnabled()

func _disableModifiers():
	_animationModifier = ""
	_animMan.setDashingColorDisabled()

func _getStatus() -> int:
	match _player.getMainFSM().getCurrentStateID():
		Player.State.RUNNING, \
		Player.State.NORMAL_JUMP_START, Player.State.WALL_JUMP_START_1: # Be forgiving when jumping
			return Status.CAN_START_AND_CONTINUE
		Player.State.WALL_SLIDING:
			return Status.CAN_START
		Player.State.WALL_JUMP_START_2, Player.State.JUMP_CONTINUE, Player.State.FALLING:
			return Status.CAN_CONTINUE
		_:
			return Status.MUST_STOP

func _startDashing():
	Debugger.verboseLog("DASHING modifier added")
	_enableModifiers()
	_animMan.notifyAnimationChanged()
	_trailMan.start()
	_camera.startShake()
	SFManager.play(SFManager.DASH)

func _stopDashing():
	Debugger.verboseLog("DASHING modifier removed")
	_disableModifiers()
	_animMan.notifyAnimationChanged()
	if _player.getMainFSM().getCurrentStateID() != Player.State.SLIDING:
		_trailMan.stop()
