# © 2020 Axel L. <ael dot pm at protonmail dot com>

extends Node

const FADE_OPACITY_START = 0.5
const FADE_SECONDS = 0.5
const TRAIL_COUNT = 11 # Measured based on 0.5-second tween durations

onready var _animSpriteOutline: AnimatedSprite = $"../Visuals/AnimatedSprites/Outline"
onready var _animSpriteNoOutline: AnimatedSprite = $"../Visuals/AnimatedSprites/NoOutline"
var _trailTimer = SimpleTimer.new(0.05)
var _trailSprites = []
var _nextTrailIndex = 0

func _ready(): # Override
	_trailSprites.resize(TRAIL_COUNT)
	for i in TRAIL_COUNT:
		_trailSprites[i] = Sprite.new()
		_trailSprites[i].modulate.a = 0.0
		var tween = Tween.new()
		_trailSprites[i].add_child(tween)
		add_child(_trailSprites[i])

func _process(dt: float): # Override
	if !_trailTimer.hasStarted(): return
	_trailTimer.update(dt)
	if _trailTimer.hasReachedTimeLimit():
		_addTrailSprite()
		_trailTimer.start()

func start():
	_trailTimer.start()

func stop():
	_trailTimer.stop()

func _addTrailSprite():
	var sprite = _trailSprites[_nextTrailIndex]
	var tween: Tween = sprite.get_child(0)
	_nextTrailIndex += 1
	_nextTrailIndex %= _trailSprites.size()

	sprite.modulate.a = 0.0
	sprite.texture = \
		_animSpriteOutline.frames.get_frame(_animSpriteOutline.animation, _animSpriteOutline.frame)
	sprite.position = get_parent().position
	sprite.flip_h = _animSpriteOutline.flip_h
	sprite.modulate = _animSpriteNoOutline.modulate

	Utils.trueOrError(tween.stop_all())
	Utils.trueOrError(tween.interpolate_property(sprite, "modulate:a", FADE_OPACITY_START, 0.0, \
		FADE_SECONDS, Tween.TRANS_LINEAR))
	Utils.trueOrError(tween.start())
