# © 2020 Axel L. <ael dot pm at protonmail dot com>

# TODO Refactor

extends KinematicBody2D
class_name Player

##############################################
### Information for the current frame only ###
##############################################

class FrameInfo:
	# The number of seconds elapsed since the last frame.
	var dt: float

	# The input direction for this frame.
	var inputDir: float

	# Is the player on the floor?
	var isGrounded: bool

	# Self-explanatory. Set by a state.
	var shouldMoveRegardlessOfInputDir: bool

var _frameInfo = FrameInfo.new()

func getFrameInfo() -> FrameInfo:
	return _frameInfo

##########################
### Main state machine ###
##########################

enum State {
	IDLE,
	RUNNING,
	NORMAL_JUMP_START,
	WALL_JUMP_START_1,
	WALL_JUMP_START_2,
	JUMP_CONTINUE,
	FALLING,
	WALL_SLIDING,
	SLIDING,
	CROUCHING,
	_SIZE
}

const STATE_NAMES = [
	"IDLE",
	"RUNNING",
	"NORMAL_JUMP_START",
	"WALL_JUMP_START_1",
	"WALL_JUMP_START_2",
	"JUMP_CONTINUE",
	"FALLING",
	"WALL_SLIDING",
	"SLIDING",
	"CROUCHING"
]

var _mainFSM = FSM.new(State._SIZE)

func getMainFSM():
	return _mainFSM

##############################
### Power-up state machine ###
##############################

onready var _powerUpFSM = $PowerUpFSM

func hasAirJump() -> bool:
	return _powerUpFSM.hasAirJump()

func removeAirJump():
	_powerUpFSM.removeAirJump()

#################################
### Stats and other constants ###
#################################

class Stats:
	# When grounded, the vertical velocity must be a large enough
	# positive value to prevent problems with 'is_grounded'.
	const Y_VELOCITY_GROUNDED = 10.0

	var gravityJumping = 1500.0
	var gravityFalling = 3000.0
	var yVelocityJumping = -800.0
	var yVelocityWallJumping = yVelocityJumping
	var xSpeedRunning = 500.0
	var xSpeedDashing = xSpeedRunning * 1.8
	var xSpeedSlidingStart = xSpeedDashing
	var xSpeedCrouching = 200.0
	var xSpeedInAir = xSpeedRunning
	var xSpeedWallJumpingStart = 200.0

var stats = Stats.new()

#############################
### Related to x-velocity ###
#############################

const ACCEL_AND_FRICTION_X = 0.1

var _isAccelDisabledX = false
var _velX = 0.0

func _updateVelocityX():
	var moveDirX = _faceDir if _frameInfo.shouldMoveRegardlessOfInputDir else _frameInfo.inputDir
	var targetVelX = (stats.xSpeedDashing if _dashMan.isDashing() \
		else _mainFSM.getCurrentState().getTargetSpeedX()) * moveDirX
	_velX = targetVelX if _isAccelDisabledX else lerp(_velX, targetVelX, ACCEL_AND_FRICTION_X)

func getVelocityX() -> float:
	return _velX

func setAccelerationDisabledX(isDisabled: bool):
	_isAccelDisabledX = isDisabled

#############################
### Related to y-velocity ###
#############################

# The vertical velocity of the player for the upcoming frame. Down is >0.
var _velY = 0.0

var _gravity: float

func applyGravity():
	_velY += _gravity * _frameInfo.dt

func applyGravityWithModifier(modifier: float):
	_velY += _gravity * modifier * _frameInfo.dt

func setVelocityY(velY: float):
	_velY = velY

func isVelocityFalling() -> bool:
	return (_velY >= 0.0)

func setGravityJumping():
	_gravity = stats.gravityJumping

func setGravityFalling():
	_gravity = stats.gravityFalling

func isGravityJumping() -> bool:
	return (_gravity == stats.gravityJumping)

#################################
### Related to face direction ###
#################################

# The direction in which the player is currently facing: -1 (left) or +1 (right).
var _faceDir = 1.0

func flipFaceDirection():
	_faceDir = -_faceDir
	_animMan.flip()
	_dashMan.notifyFaceDirectionFlipped()

func getFaceDirection() -> float:
	return _faceDir

#################
### Main code ###
#################

onready var _animMan = $"Visuals/AnimationManager"
onready var _dashMan = $DashManager
onready var _startPos = position

func _ready(): # Override
	LevelResetter.registerPlayer(self)
	_mainFSM.enter(State.FALLING)
	_dashMan.notifyMainStateChange()
	setGravityFalling()
	Utils.connectSignal(Nodes.getLevelManager(), "goalReached", self, "_onGoalReached")
	Utils.connectSignal(Nodes.getLevelManager(), "playerKilled", self, "_onDeath")

func _physics_process(dt: float): # Override
	_frameInfo.dt = dt
	_frameInfo.shouldMoveRegardlessOfInputDir = _dashMan.isDashing()

	# Get the direction input by the player this frame. (+1 for right, -1 for left.)
	_frameInfo.inputDir = 0.0
	if Input.is_action_pressed("moveRight"): _frameInfo.inputDir += 1.0
	if Input.is_action_pressed("moveLeft"): _frameInfo.inputDir -= 1.0

	# After the previous frame's movement but before this one's, is the player on the ground?
	_frameInfo.isGrounded = is_on_floor()

	while (true):
		var newState = _mainFSM.getCurrentState().update()
		if newState == -1: break
		_mainFSM.enter(newState)
		_dashMan.notifyMainStateChange()
		_powerUpFSM.notifyMainStateChange()

	_dashMan.update()
	_updateVelocityX()
	var _vec = move_and_slide(Vector2(_velX, _velY), Vector2(.0, -1.0))

	for i in get_slide_count(): # The same collider can frequently be found twice, for some reason
		var collision: KinematicCollision2D = get_slide_collision(i)
		if collision.collider.collision_layer & ConfigManager.LAYER_BIT_DAMAGING:
			Nodes.getLevelManager().restart()
			break

func _onDeath(): # Called by signal
	_stopAndFadeOut(1.0)
	$NormalCollisionShape.queue_free()
	$SlidingCollisionShape.queue_free()
	SFManager.play(SFManager.PLAYER_DEATH)

	# Move the player back to the starting location.
	$Camera.smoothing_enabled = false
	var tween = Tween.new()
	add_child(tween)
	Utils.trueOrError(tween.interpolate_property(self, "position", position, \
		_startPos, 1.0 * Engine.time_scale, Tween.EASE_OUT))
	Utils.trueOrError(tween.start())

func _onGoalReached(): # Called by signal
	_stopAndFadeOut(1.0)

func _stopAndFadeOut(fadeDuration: float):
	_animMan.fadeOut(fadeDuration)
	_dashMan.forceStop()
	$SlowdownManager.set_process_input(false) # Don't free; doing so sets the time scale to 1
	set_physics_process(false)
