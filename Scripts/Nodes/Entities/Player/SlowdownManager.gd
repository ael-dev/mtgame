# © 2020 Axel L. <ael dot pm at protonmail dot com>

extends Node

const _COLOR_SHOW = Color(0.8, 0.2, 0.2, 0.2)
const _COLOR_HIDE = Color(1.0, 1.0, 1.0, 0.0)

onready var _colorRect = $CanvasLayer/ColorRect
var _tween = Tween.new()

func _ready():
	_colorRect.color = _COLOR_HIDE
	add_child(_tween)
	Utils.connectSignal(self, "tree_exiting", self, "_onExiting")

func _input(event: InputEvent): # Override
	if event is InputEventKey and event.is_action("slowTime"):
		if event.pressed:
			if !ConfigManager.shouldToggleSlowdown:
				TimeScaleManager.setTimeScaleMultiplier(ConfigManager.slowdownMultiplier)
				_tweenShow()
		else:
			if ConfigManager.shouldToggleSlowdown:
				if TimeScaleManager.getTimeScaleMultiplier() == 1.0:
					TimeScaleManager.setTimeScaleMultiplier(ConfigManager.slowdownMultiplier)
					_tweenShow()
				else:
					TimeScaleManager.setTimeScaleMultiplier(1.0)
					_tweenHide()
			else:
				TimeScaleManager.setTimeScaleMultiplier(1.0)
				_tweenHide()

func _onExiting(): # Called by signal
	TimeScaleManager.setTimeScaleMultiplier(1.0)

func _doTween(startColor: Color, endColor: Color):
	_tween.interpolate_property(_colorRect, "color", startColor, endColor, \
		0.2 * ConfigManager.slowdownMultiplier, Tween.TRANS_LINEAR, Tween.EASE_IN)
	_tween.start()
func _tweenHide(): _doTween(_COLOR_SHOW, _COLOR_HIDE)
func _tweenShow(): _doTween(_COLOR_HIDE, _COLOR_SHOW)
