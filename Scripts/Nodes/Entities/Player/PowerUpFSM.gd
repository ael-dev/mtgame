# © 2020 Axel L. <ael dot pm at protonmail dot com>

# A state machine which stores the player's current power-up.

extends Node

enum State { NONE, AIR_JUMP }

const _STATE_NAMES = [ "NONE", "AIR_JUMP" ]

var _currentStateID = State.NONE

func notifyMainStateChange():
	match _currentStateID:
		State.AIR_JUMP:
			match get_parent().getMainFSM().getCurrentStateID():
				Player.State.NORMAL_JUMP_START: pass
				Player.State.WALL_JUMP_START_1, Player.State.WALL_JUMP_START_2: pass
				Player.State.JUMP_CONTINUE: pass
				Player.State.FALLING: pass
				_: _enter(State.NONE)
		_: pass

func _enter(newStateID: int):
	Debugger.verboseLog("New power-up state: " + _STATE_NAMES[newStateID])
	_currentStateID = newStateID

#####################
## State: AIR_JUMP ##
#####################

func giveAirJump():
	_enter(State.AIR_JUMP)
func hasAirJump() -> bool:
	return (_currentStateID == State.AIR_JUMP)
func removeAirJump():
	assert(_currentStateID == State.AIR_JUMP)
	_enter(State.NONE)
