# © 2020 Axel L. <ael dot pm at protonmail dot com>

# Handles the player's CollisionShapes and RayCasts.

extends Node

onready var _player = get_parent()
onready var _normalShape = $"../NormalCollisionShape"
onready var _slidingShape = $"../SlidingCollisionShape"
onready var _leftWallRayCasts = $"../WallRayCasts/Left"
onready var _rightWallRayCasts = $"../WallRayCasts/Right"
onready var _slidingRayCasts = $"../SlidingRayCasts"

func _ready(): # Override
	for rc in _leftWallRayCasts.get_children():
		rc.add_exception(_player)
	for rc in _rightWallRayCasts.get_children():
		rc.add_exception(_player)
	for rc in _slidingRayCasts.get_children():
		rc.add_exception(_player)

func canStopSliding() -> bool:
	for rc in _slidingRayCasts.get_children():
		if rc.is_colliding():
			return false
	return true

func isCollidingWithWall() -> bool:
	var rayCasts: Node2D
	match _player.getFaceDirection():
		-1.0: rayCasts = _leftWallRayCasts
		_: rayCasts = _rightWallRayCasts
	for rc in rayCasts.get_children():
		if rc.is_colliding():
			return true
	return false

func setNormalShape():
	_normalShape.disabled = false
	_slidingShape.disabled = true

func setSlidingShape():
	_normalShape.disabled = true
	_slidingShape.disabled = false
