# © 2020 Axel L. <ael dot pm at protonmail dot com>

extends PlayerState

func _ready(): # Override
	._register(Player.State.RUNNING, _player.stats.xSpeedRunning)

func enter(_prevStateID: int):
	._enter()
	if (_fi.inputDir != _player.getFaceDirection()):
		_player.flipFaceDirection()
	_animMan.notifyAnimationChanged()
	_animMan.updateColorMultiplier(Color(1.0, 1.0, 1.0))

func exit(): pass

func update() -> int:
	if !_fi.isGrounded:
		return Player.State.FALLING

	if Input.is_action_just_pressed("jump"):
		return Player.State.NORMAL_JUMP_START

	if Input.is_action_pressed("slide"):
		return Player.State.SLIDING

	if _fi.inputDir == 0.0 and !_fi.shouldMoveRegardlessOfInputDir:
		return Player.State.IDLE

	# In case the player turns around in a single frame (without
	# transitioning to IDLE), reverse the face direction.
	if _fi.inputDir == -_player.getFaceDirection():
		_player.flipFaceDirection()

	return -1
