# © 2020 Axel L. <ael dot pm at protonmail dot com>

extends PlayerState

onready var _collisionMan = _player.get_node("CollisionManager")
onready var _squishMan = _player.get_node("Visuals/SquishManager")
var _coyoteTimer = SimpleTimer.new(0.1)
var _jumpBuffer = JumpBuffer.new()
var _shouldMoveForward: bool

func _ready(): # Override
	._register(Player.State.FALLING, _player.stats.xSpeedInAir)

func enter(prevStateID: int):
	._enter()
	_animMan.notifyAnimationChanged()
	_shouldMoveForward = _fi.shouldMoveRegardlessOfInputDir
	_jumpBuffer.reset()

	match prevStateID:
		Player.State.RUNNING, Player.State.SLIDING, Player.State.WALL_SLIDING:
			_coyoteTimer.start()

func exit():
	_coyoteTimer.stop()

func update() -> int:
	_jumpBuffer.update(_fi.isGrounded)

	if _coyoteTimer.hasStarted() and !_coyoteTimer.hasReachedTimeLimit():
		if Input.is_action_just_pressed("jump"):
			return Player.State.NORMAL_JUMP_START

	if _fi.isGrounded:
		_player.setVelocityY(_player.stats.Y_VELOCITY_GROUNDED)
		_squishMan.land()
		if _jumpBuffer.hasJump():
			return Player.State.NORMAL_JUMP_START
		return Player.State.IDLE

	if _fi.inputDir != -_player.getFaceDirection():
		if _collisionMan.isCollidingWithWall():
			if _jumpBuffer.hasJump():
				return Player.State.WALL_JUMP_START_1
			return Player.State.WALL_SLIDING
	elif _fi.inputDir == -_player.getFaceDirection():
		_player.flipFaceDirection()
		_shouldMoveForward = false

	if _player.hasAirJump() and Input.is_action_just_pressed("jump"):
		_player.removeAirJump()
		return Player.State.NORMAL_JUMP_START

	_player.applyGravity()
	_coyoteTimer.update(_fi.dt)
	_fi.shouldMoveRegardlessOfInputDir = _shouldMoveForward

	return -1
