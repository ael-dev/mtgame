# © 2020 Axel L. <ael dot pm at protonmail dot com>

# The base class for each of the player's states.

extends Node
class_name PlayerState

onready var _player = get_parent().get_parent()
onready var _animMan = _player.get_node("Visuals/AnimationManager")
onready var _fi = _player.getFrameInfo()
var _targetSpeedX: float
var _name: String

# Called by all non-abstract subclasses in 'enter'.
func _enter():
	assert (_name != null)
	Debugger.verboseLog("New state: " + _name)

# Called by all non-abstract subclasses in '_ready'.
func _register(stateID: int, targetSpeedX: float):
	_name = Player.STATE_NAMES[stateID]
	_targetSpeedX = targetSpeedX
	_player.getMainFSM().registerState(stateID, self)

func getTargetSpeedX() -> float:
	return _targetSpeedX
