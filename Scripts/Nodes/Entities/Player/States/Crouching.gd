# © 2020 Axel L. <ael dot pm at protonmail dot com>

extends PlayerState

onready var _collisionMan = _player.get_node("CollisionManager")

func _ready(): # Override
	._register(Player.State.CROUCHING, _player.stats.xSpeedCrouching)

func enter(_prevStateID: int):
	._enter()
	_animMan.notifyAnimationChanged()
	_collisionMan.setSlidingShape()
	_animMan.updateColorMultiplier(Color(1.5, .0, .0))

func exit():
	_collisionMan.setNormalShape()

func update() -> int:
	return Player.State.RUNNING if _collisionMan.canStopSliding() else -1
