# © 2020 Axel L. <ael dot pm at protonmail dot com>

# Dashing is NOT enabled for this state, unlike #1.

extends PlayerStateWallJump

func _ready(): # Override
	_initialize(Player.State.WALL_JUMP_START_2, Player.State.JUMP_CONTINUE, 0.1)

func enter(_prevStateID: int):
	._enter()
