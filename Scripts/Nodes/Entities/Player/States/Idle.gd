# © 2020 Axel L. <ael dot pm at protonmail dot com>

extends PlayerState

# When the IDLE state is entered, this timer is used to wait for a very short amount of time before
# showing the animation, in order to prevent flickering between the IDLE and RUNNING states.
var _animStartTimer = SimpleTimer.new(0.05)

func _ready(): # Override
	._register(Player.State.IDLE, 0.0)

func enter(_prevStateID: int):
	._enter()
	_animStartTimer.start()

func exit(): pass

func update() -> int:
	if !_fi.isGrounded:
		Utils.printError("Not grounded when IDLE?")
		return Player.State.FALLING

	if _fi.inputDir != 0.0:
		return Player.State.RUNNING

	if Input.is_action_just_pressed("dash"):
		_fi.inputDir = _player.getFaceDirection()
		if Input.is_action_just_pressed("jump"):
			return Player.State.NORMAL_JUMP_START
		return Player.State.RUNNING

	if Input.is_action_just_pressed("jump"):
		return Player.State.NORMAL_JUMP_START

	if Input.is_action_just_pressed("slide"):
		return Player.State.SLIDING

	if _animStartTimer.hasStarted():
		_animStartTimer.update(_fi.dt)
		if _animStartTimer.hasReachedTimeLimit():
			_animMan.notifyAnimationChanged()
			_animStartTimer.stop()
			_animMan.updateColorMultiplier(Color(1.0, 1.0, 1.0))

	return -1
