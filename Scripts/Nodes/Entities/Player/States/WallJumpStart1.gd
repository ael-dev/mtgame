# © 2020 Axel L. <ael dot pm at protonmail dot com>

# Dashing is enabled for this state, unlike #2.

extends PlayerStateWallJump

func _ready(): # Override
	._initialize(Player.State.WALL_JUMP_START_1, Player.State.WALL_JUMP_START_2, .05)

func enter(_prevStateID: int):
	._enter()
	_player.flipFaceDirection()
	_player.setGravityJumping()
	_animMan.updateColorMultiplier(Color(1.0, 1.0, 2.0))
	_player.setVelocityY(_player.stats.yVelocityWallJumping)
	SFManager.play(SFManager.JUMP)
