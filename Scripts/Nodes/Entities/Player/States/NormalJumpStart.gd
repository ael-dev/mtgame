# © 2020 Axel L. <ael dot pm at protonmail dot com>

extends PlayerState

onready var _collisionMan = _player.get_node("CollisionManager")
var _endTimer = SimpleTimer.new(.05)
var _jumpBuffer = JumpBuffer.new()

func _ready(): # Override
	._register(Player.State.NORMAL_JUMP_START, _player.stats.xSpeedInAir)

func enter(_prevStateID: int):
	._enter()
	_animMan.notifyAnimationChanged()
	_animMan.updateColorMultiplier(Color(1.0, 1.0, 2.0))
	_endTimer.start()
	_player.setGravityJumping()
	_player.setVelocityY(_player.stats.yVelocityJumping)
	_jumpBuffer.reset()
	SFManager.play(SFManager.JUMP)

func exit(): pass

func update() -> int:
	if Input.is_action_just_released("jump"):
		_player.setGravityFalling()
		return Player.State.JUMP_CONTINUE

	_endTimer.update(_fi.dt)
	if _endTimer.hasReachedTimeLimit():
		return Player.State.JUMP_CONTINUE

	if _fi.inputDir != -_player.getFaceDirection():
		if _collisionMan.isCollidingWithWall():
			if _jumpBuffer.hasJump():
				return Player.State.WALL_JUMP_START_1
			return Player.State.WALL_SLIDING

	elif _fi.inputDir == -_player.getFaceDirection():
		_player.flipFaceDirection()
		return Player.State.JUMP_CONTINUE

	_player.applyGravity()

	return -1
