# © 2020 Axel L. <ael dot pm at protonmail dot com>

extends PlayerState

onready var _camera = _player.get_node("Camera")
onready var _collisionMan = _player.get_node("CollisionManager")
onready var _trailMan = _player.get_node("TrailManager")
var _endTimer = SimpleTimer.new(0.8)

func _ready(): # Override
	._register(Player.State.SLIDING, 0.0) # Dynamic horizontal movement speed

func enter(_prevStateID: int):
	._enter()
	_animMan.notifyAnimationChanged()
	_collisionMan.setSlidingShape()
	_endTimer.start()
	_trailMan.start()
	_camera.startShake()
	_animMan.updateColorMultiplier(Color(2.0, .0, .0))
	SFManager.play(SFManager.SLIDE)

func exit():
	_collisionMan.setNormalShape()
	_trailMan.stop()

func update() -> int: # Override
	_targetSpeedX = _player.stats.xSpeedSlidingStart * _endTimer.getPercentageLeft()

	if _targetSpeedX <= _player.stats.xSpeedRunning:
		_trailMan.stop()

	if !_fi.isGrounded:
		return Player.State.FALLING

	var canStopSliding = _collisionMan.canStopSliding()

	if Input.is_action_just_released("slide"):
		if canStopSliding:
			return Player.State.IDLE
		return Player.State.CROUCHING

	if Input.is_action_pressed("jump") and canStopSliding:
		return Player.State.NORMAL_JUMP_START

	_endTimer.update(_fi.dt)
	if _endTimer.hasReachedTimeLimit():
		return Player.State.IDLE if canStopSliding else Player.State.CROUCHING

	_fi.shouldMoveRegardlessOfInputDir = true
	return -1
