# © 2020 Axel L. <ael dot pm at protonmail dot com>

# --- THE REASON FOR USING DIFFERENT CODE DURING THE FIRST FRAME ---
# When holding a direction and transitioning to a JUMP_START state, 'isGrounded' will still
# be true when the state machine transitions to this state. Without waiting a frame
# before deciding that the IDLE state should be entered (i.e., the player has landed),
# the IDLE state will also be entered on the same frame as this state was entered
# (when moving against a wall while jumping), which will cause an endless loop.

extends PlayerState

onready var _collisionMan = _player.get_node("CollisionManager")
onready var _squishMan = _player.get_node("Visuals/SquishManager")
var _isOnFirstFrame: bool

const GRAVITY_MULTIPLIER_UP = 0.8
const GRAVITY_MULTIPLIER_DOWN = 0.2

func _ready():
	# (Set the horizontal speed so that the animation isn't paused.)
	._register(Player.State.WALL_SLIDING, _player.stats.xSpeedRunning)

func enter(_prevStateID: int):
	._enter()
	_animMan.notifyAnimationChanged()
	_isOnFirstFrame = true
	_squishMan.hitWall()

func exit(): pass

func _applyGravity():
	_player.applyGravityWithModifier(GRAVITY_MULTIPLIER_DOWN if _player.isVelocityFalling() \
		else GRAVITY_MULTIPLIER_UP)

func update() -> int:
	if _isOnFirstFrame:
		_isOnFirstFrame = false
		_applyGravity()
		return -1
	return _updateNotFirstFrame()

func _updateNotFirstFrame() -> int: # Override
	if _fi.isGrounded:
		_player.setVelocityY(_player.stats.Y_VELOCITY_GROUNDED)
		_squishMan.land()
		return Player.State.IDLE

	else:
		if Input.is_action_just_pressed("jump"):
			_squishMan.leaveWall()
			return Player.State.WALL_JUMP_START_1

		if _fi.inputDir == -_player.getFaceDirection():
			_player.flipFaceDirection()
			_squishMan.leaveWall()
			return Player.State.FALLING

		if !_collisionMan.isCollidingWithWall():
			_squishMan.leaveWall()
			return Player.State.FALLING

	_applyGravity()
	return -1
