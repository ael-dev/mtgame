# © 2020 Axel L. <ael dot pm at protonmail dot com>

extends PlayerState

onready var _collisionMan = _player.get_node("CollisionManager")
var _jumpBuffer = JumpBuffer.new()
var _shouldMoveForward: bool

func _ready(): # Override
	._register(Player.State.JUMP_CONTINUE, _player.stats.xSpeedInAir)

func enter(prevStateID: int):
	._enter()
	_animMan.notifyAnimationChanged()
	_shouldMoveForward = _fi.shouldMoveRegardlessOfInputDir
	_jumpBuffer.reset()

	if _player.isGravityJumping() and !Input.is_action_pressed("jump"):
		assert(prevStateID in [ \
			Player.State.FALLING, \
			Player.State.JUMP_CONTINUE, \
			Player.State.WALL_JUMP_START_2 \
		])
		_player.setGravityFalling()

func exit():
	_player.setGravityFalling()

func update() -> int:
	_jumpBuffer.update(_fi.isGrounded)

	if _player.isGravityJumping() and Input.is_action_just_released("jump"):
		_player.setGravityFalling()

	if _fi.inputDir != -_player.getFaceDirection():
		if _collisionMan.isCollidingWithWall():
			if _jumpBuffer.hasJump():
				return Player.State.WALL_JUMP_START_1
			return Player.State.WALL_SLIDING

	elif _fi.inputDir == -_player.getFaceDirection():
		_player.flipFaceDirection()
		_shouldMoveForward = false

	_fi.shouldMoveRegardlessOfInputDir = _shouldMoveForward

	if _player.isVelocityFalling():
		return Player.State.FALLING

	if _player.hasAirJump() and Input.is_action_just_pressed("jump"):
		_player.removeAirJump()
		return Player.State.NORMAL_JUMP_START

	_player.applyGravity()

	return -1
