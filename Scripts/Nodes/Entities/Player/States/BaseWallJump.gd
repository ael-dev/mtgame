# © 2020 Axel L. <ael dot pm at protonmail dot com>

extends PlayerState
class_name PlayerStateWallJump

onready var _collisionMan = _player.get_node("CollisionManager")
var _endStateID: int
var _endTimer: SimpleTimer

func _initialize(stateID: int, endStateID: int, timerDuration: float):
	._register(stateID, _player.stats.xSpeedWallJumpingStart)
	_endStateID = endStateID
	_endTimer = SimpleTimer.new(timerDuration)

func _enter(): # Override
	._enter()
	_endTimer.start()
	_animMan.notifyAnimationChanged()
	_player.setAccelerationDisabledX(true)

func exit():
	_player.setAccelerationDisabledX(false)

func update() -> int:
	_fi.shouldMoveRegardlessOfInputDir = true

	if _collisionMan.isCollidingWithWall():
		return Player.State.WALL_SLIDING

	_endTimer.update(_fi.dt)
	if _endTimer.hasReachedTimeLimit():
		return _endStateID

	_player.applyGravity()

	return -1
