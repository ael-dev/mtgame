# © 2020 Axel L. <ael dot pm at protonmail dot com>

# Handles the visual "squishing" of the player.

extends Node

class TweenInfo:
	var duration: float
	var posPair: Array # <Vector2>
	var scalePair: Array # <Vector2>

	func _init(_duration: float, _posPair: Array, _scalePair: Array):
		duration = _duration
		posPair = _posPair
		scalePair = _scalePair

const REVERSE = true
const NO_REVERSE = false

onready var _animSprites: Array = $"../AnimatedSprites".get_children()
onready var _tween: Tween = $Tween
var _tweenInfoLand = TweenInfo.new(
	0.1,
	[ Vector2(0.0, 0.0), Vector2(0.0, 7.0) ],
	[ Vector2(1.0, 1.0), Vector2(1.2, 0.8) ]
)
var _tweenInfoHitWall = TweenInfo.new(
	0.2,
	[ Vector2(0.0, 0.0), Vector2(4.0, 0.0) ],
	[ Vector2(1.0, 1.0), Vector2(0.9, 1.1) ]
)

func _onLandDone(): # Called by signal
	_doTween(_tweenInfoLand, REVERSE)
	Utils.disconnectSignal(_tween, "tween_all_completed", self, "_onLandDone")

func hitWall():
	_doTween(_tweenInfoHitWall, NO_REVERSE)
func land():
	_doTween(_tweenInfoLand, NO_REVERSE)
	Utils.connectSignal(_tween, "tween_all_completed", self, "_onLandDone")
func leaveWall():
	_doTween(_tweenInfoHitWall, REVERSE)

func _doTween(info: TweenInfo, shouldReverse: bool):
	var startIndex = 1 if shouldReverse else 0
	var endIndex = (startIndex + 1) % 2

	var startPos = info.posPair[startIndex]
	var endPos = info.posPair[endIndex]
	if _animSprites[0].flip_h:
		startPos.x *= -1
		endPos.x *= -1

	for a in _animSprites:
		Utils.trueOrError(_tween.interpolate_property(a, "position", startPos, endPos, info.duration))
		Utils.trueOrError(_tween.interpolate_property(a, "scale", info.scalePair[startIndex], \
			info.scalePair[endIndex], info.duration))

	Utils.trueOrError(_tween.start())
