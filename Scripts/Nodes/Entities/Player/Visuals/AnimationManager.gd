# © 2020 Axel L. <ael dot pm at protonmail dot com>

# Handles the visual aspects of the player, excluding the "squishing".

extends Node

const _DASH_COLOR = Color(1.0, 2.0, 1.0)

onready var _animSprites: Array = $"../AnimatedSprites".get_children()
onready var _colorTween: Tween = $ColorTween
var _currentStateColorMultiplier = null
onready var _dashMan = Nodes.getPlayer().get_node("DashManager")
onready var _player = Nodes.getPlayer()
var _prevStateID = -1

func _physics_process(_dt: float): # Override
	for a in _animSprites:
		var tsx = _player.getMainFSM().getCurrentState().getTargetSpeedX()
		a.speed_scale = stepify(abs(_player.getVelocityX()) / tsx if tsx != 0.0 else 0.0, 0.1)

func fadeOut(duration):
	var tween = Tween.new()
	add_child(tween)
	for a in _animSprites:
		Utils.trueOrError(tween.interpolate_property(a, "modulate:a", \
			1.0, 0.0, duration * Engine.time_scale))
	tween.start()

func flip():
	for a in _animSprites:
		a.flip_h = !a.flip_h

func getAnimationFrameCount(animName: String) -> int:
	return _animSprites[0].get_frame_count(animName)

func getCurrentAnimationFrame() -> int:
	return _animSprites[0].frame

func notifyAnimationChanged():
	match _player.getMainFSM().getCurrentStateID():
		Player.State.SLIDING:
			_setAnimation("Sliding")
		Player.State.CROUCHING:
			_setAnimation("Crouching")
		_:
			_setAnimation("Rolling")

func setDashingColorDisabled():
	_setColorMultiplier(_currentStateColorMultiplier)

func setDashingColorEnabled():
	_setColorMultiplier(_DASH_COLOR * _currentStateColorMultiplier)

func updateColorMultiplier(multiplier: Color):
	if _player.getMainFSM().getCurrentStateID() != _prevStateID:
		_currentStateColorMultiplier = multiplier
		_prevStateID = _player.getMainFSM().getCurrentStateID()
		_setColorMultiplier((_DASH_COLOR * multiplier) if _dashMan.isDashing() else multiplier)

func _setAnimation(animName: String):
	if _animSprites[0].animation != animName:
		for a in _animSprites:
			a.play(animName)

func _setColorMultiplier(color: Color):
	Utils.trueOrError(_colorTween.stop(_animSprites[1]))
	Utils.trueOrError(_colorTween.interpolate_property(_animSprites[1], "modulate", \
		_animSprites[1].modulate, color, 0.2))
	Utils.trueOrError(_colorTween.start())
