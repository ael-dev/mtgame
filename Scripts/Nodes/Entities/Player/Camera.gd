# © 2020 Axel L. <ael dot pm at protonmail dot com>

# Manages camera shake and camera smoothing, if these are enabled.

extends Camera2D

onready var _camOffsetY = offset.y
var _shakeTimer = SimpleTimer.new(0.1)

func _ready(): # Override
	Utils.connectSignal(ConfigManager, "reloadConfig", self, "_onConfigReloaded")
	_updateSmoothingSpeed()

func _process(dt: float): # Override
	if _shakeTimer.hasStarted():
		_shakeTimer.update(dt)
		if _shakeTimer.hasReachedTimeLimit():
			_shakeTimer.stop()
		else:
			offset = Vector2(_getRandomCameraOffset(), _getRandomCameraOffset() + _camOffsetY)

func _onConfigReloaded(): # Called by signal
	_updateSmoothingSpeed()

func startShake():
	if ConfigManager.isShakeEnabled:
		_shakeTimer.start()

func _getRandomCameraOffset() -> float:
	return (rand_range(-5.0, 5.0))

func _updateSmoothingSpeed():
	if ConfigManager.smoothingSpeed != 0.0:
		Debugger.verboseLog("Camera smoothing turned on")
		smoothing_enabled = true
		smoothing_speed = ConfigManager.smoothingSpeed
	else:
		Debugger.verboseLog("Camera smoothing turned off")
		smoothing_enabled = false

	zoom.x = ConfigManager.fovMultiplier
	zoom.y = zoom.x
