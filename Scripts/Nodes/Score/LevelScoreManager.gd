# © 2020 Axel L. <ael dot pm at protonmail dot com>

extends CanvasLayer

const _STR_FORMAT = " [wave]" # (The space is intentional)

onready var _scoreLabel: Label = $ScoreLabel
var _elapsedTime: float
var _updateTimer = Timer.new()

func _ready(): # Override
	LevelResetter.registerState(self)
	_scoreLabel.bbcode_text = _STR_FORMAT + "0"
	Utils.connectSignal($StartingArea, "body_entered", self, "_onStartingAreaEntered")
	Utils.connectSignal(Nodes.getLevelManager(), "goalReached", self, "_onGoalReached")

func _physics_process(dt: float): # Override
	# Can't use timestamps without a more complex system because it counts time in the pause menu.
	_elapsedTime += dt / Engine.time_scale

func _onGoalReached(): # Called by signal
	_updateTimer.stop()

func _onStartingAreaEntered(_body): # Called by signal
	$StartingArea.queue_free()
	_elapsedTime = 0.0
	add_child(_updateTimer)
	Utils.connectSignal(_updateTimer, "timeout", self, "_updateHUD")
	_updateTimer.start(0.1)

func _updateHUD(): # Called by signal
	_scoreLabel.bbcode_text = str(_STR_FORMAT, stepify(_elapsedTime, 0.1))
