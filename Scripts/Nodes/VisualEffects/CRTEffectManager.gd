# © 2020 Axel L. <ael dot pm at protonmail dot com>

extends Node

onready var _colorRect = $ColorRect

func _ready(): # Override
	_update()
	Utils.connectSignal(ConfigManager, "reloadConfig", self, "_update")

func _update(): # Called by signal and '_ready'
	_colorRect.visible = ConfigManager.isCRTEffectEnabled
