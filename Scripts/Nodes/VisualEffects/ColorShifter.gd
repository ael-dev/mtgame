# © 2020 Axel L. <ael dot pm at protonmail dot com>

extends Node

enum NodeType { ANIMATED_SPRITE, COLOR_RECT, LABEL, RICH_TEXT_LABEL }

export var minimumDuration = 0.2
export var maximumDuration = 1.0
export var alpha = 1.0
export(NodeType) var nodeType = NodeType.ANIMATED_SPRITE

onready var _colors = [
	Color(.20, .20, .80, alpha),
	Color(.20, .80, .20, alpha),
	Color(.80, .20, .20, alpha)
]

var _nodeToShift: Node
var _propertyToShift: String
var _sourceColorIndex
var _targetColorIndex
var _tween = Tween.new()

# Do this in '_process' instead of in '_ready' to avoid problems with spawning.
func _process(_dt: float): # Override
	_targetColorIndex = randi() % _colors.size()

	match nodeType:
		NodeType.ANIMATED_SPRITE: _propertyToShift = "modulate"
		NodeType.COLOR_RECT:      _propertyToShift = "color"
		NodeType.LABEL:           _propertyToShift = "custom_colors/font_color"
		NodeType.RICH_TEXT_LABEL: _propertyToShift = "custom_colors/default_color"

	_nodeToShift = $".."
	_nodeToShift.set(_propertyToShift, _colors[_targetColorIndex])

	add_child(_tween)
	Utils.connectSignal(_tween, "tween_all_completed", self, "_startTween")
	_targetColorIndex = randi() % _colors.size()
	_startTween()
	set_process(false)

func _startTween(): # Called by signal and in '_ready'
	_sourceColorIndex = _targetColorIndex
	_targetColorIndex = (_targetColorIndex + 1) % _colors.size()
	var duration = rand_range(minimumDuration, maximumDuration)
	_tween.interpolate_property(_nodeToShift, _propertyToShift, \
		_colors[_sourceColorIndex], _colors[_targetColorIndex], duration)
	_tween.start()
