# © 2020 Axel L. <ael dot pm at protonmail dot com>

extends AnimatedSprite

# This node is used to start an AnimatedSprite's default
# animation, to prevent it from playing in the editor.

func _ready():
	playing = true
