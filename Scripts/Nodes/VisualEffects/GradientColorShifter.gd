# © 2020 Axel L. <ael dot pm at protonmail dot com>

extends TextureRect

const _BG_COLORS = [
	Color(.20, .20, .40),
	Color(.20, .40, .20),
	Color(.40, .20, .20)
]

const _TRANSITION_SECONDS = 3.0

var _sourceColorIndex = 0
var _switchTimer = SimpleTimer.new(_TRANSITION_SECONDS)
var _targetColorIndex = 1

func _ready():
	visible = true
	texture.gradient.set_color(0, _BG_COLORS[0])
	_switchTimer.start()

func _process(dt: float):
	_switchTimer.update(dt)
	if _switchTimer.hasReachedTimeLimit():
		_switchTimer.start()
		texture.gradient.set_color(0, _BG_COLORS[_targetColorIndex])
		_sourceColorIndex = _targetColorIndex
		_targetColorIndex = (_targetColorIndex + 1) % _BG_COLORS.size()
	else:
		texture.gradient.set_color(0, _BG_COLORS[_sourceColorIndex].linear_interpolate( \
			_BG_COLORS[_targetColorIndex], \
			_switchTimer.getPercentageElapsed()
		))
