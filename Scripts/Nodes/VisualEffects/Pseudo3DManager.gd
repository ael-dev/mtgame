# © 2020 Axel L. <ael dot pm at protonmail dot com>

# Manages the pseudo-3D effect in a level.

extends Node

class LayerInfo:
	var instance: CanvasLayer
	var layer: int
	var factorMultiplier: int

	func _init(layerID: int, pseudo3DFactorMultiplier: int):
		layer = layerID
		factorMultiplier = pseudo3DFactorMultiplier

const _MAX_FACTOR = 0.03

var _factor: float
var _layerInfoArray = [
	LayerInfo.new(-1, -1), LayerInfo.new(-2, -2), # Background
	LayerInfo.new(+2, +1), LayerInfo.new(+3, +2)  # Foreground
]

func _ready(): # Override
	Utils.connectSignal(ConfigManager, "reloadConfig", self, "_onConfigReloaded")

	_factor = ConfigManager.pseudo3DFactor
	if _factor != 0.0:
		if _factor < 0.0:
			Debugger.verboseLog("A negative pseudo-3D factor is invalid")
		elif _factor > _MAX_FACTOR:
			Debugger.verboseLog("A pseudo-3D factor greater than " + String(_MAX_FACTOR) + "is invalid")
		else:
			_setup()

func _onConfigReloaded(): # Called by signal
	if ConfigManager.pseudo3DFactor != _factor:
		_reconfigure()

func _reconfigure():
	if _factor != 0.0:
		if ConfigManager.pseudo3DFactor == 0.0:
			for info in _layerInfoArray:
				info.instance.queue_free()
		else:
			for info in _layerInfoArray:
				_updateScale(info)
	else: _setup()
	_factor = ConfigManager.pseudo3DFactor

func _setup():
	var canvasLayer = $"../CanvasLayer"

	for info in _layerInfoArray:
		info.instance = canvasLayer.duplicate()
		info.instance.layer = info.layer
		_updateScale(info)
		for node in info.instance.get_children():
			if node is TileMap:
				node.collision_layer = 0
				node.collision_mask = 0
		add_child(info.instance)

func _updateScale(info: LayerInfo):
	info.instance.follow_viewport_scale = 1 + (ConfigManager.pseudo3DFactor * info.factorMultiplier)
