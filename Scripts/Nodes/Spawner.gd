# © 2020 Axel L. <ael dot pm at protonmail dot com>

# Used to check if a node should be spawned.

extends Node2D

# The minimum distance (squared) required from the player
# to the spawn position for 'shouldSpawn' to return true.
var _minDistanceSquared

var _player

# The center position of the object to spawn.
var _spawnPosGlobal: Vector2

func _ready(): # Override
	_spawnPosGlobal = get_parent().global_position
	_minDistanceSquared = (get_viewport_rect().size / 2).length_squared()
	_player = Nodes.getPlayer()

func _physics_process(_dt: float): # Override
	var distanceToPlayerSquared = _spawnPosGlobal.distance_squared_to(_player.global_position)
	if distanceToPlayerSquared <= _minDistanceSquared:
		get_parent().spawn()
		queue_free()
