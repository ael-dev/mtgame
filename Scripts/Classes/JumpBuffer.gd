# © 2020 Axel L. <ael dot pm at protonmail dot com>

class_name JumpBuffer

# Stores the timestamp at which the "jump" button was last pressed.
# Reset to INF whenever the "jump" button is released.
var _lastJumpPressTimestamp = 0

# Returns true if the amount of time elapsed since the "jump" button was
# last pressed (but not released) is less than 100 milliseconds.
func hasJump():
	if _lastJumpPressTimestamp == 0: return false
	return ((OS.get_ticks_msec() - _lastJumpPressTimestamp) <= 100)

func reset():
	_lastJumpPressTimestamp = 0

func update(isGrounded: bool):
	if isGrounded:
		if Input.is_action_just_released("jump"):
			reset()
	else:
		if Input.is_action_just_pressed("jump"):
			_lastJumpPressTimestamp = OS.get_ticks_msec()
