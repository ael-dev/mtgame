# © 2020 Axel L. <ael dot pm at protonmail dot com>

# Represents a finite state machine which has been slightly
# modified so that a state is aware of the previous state.

class_name FSM

var _crtStateID: int
var _crtState = null
var _states = []

func _init(numStates: int): # Constructor
	_states.resize(numStates)

func enter(newStateID: int):
	assert (newStateID <= _states.size())
	if (_crtState != null): _crtState.exit()
	var prevStateID = _crtStateID

	# Set the state object and ID before entering the new state.
	_crtState = _states[newStateID]
	_crtStateID = newStateID

	_crtState.enter(prevStateID)

func getCurrentStateID() -> int:
	assert (_crtState != null)
	return _crtStateID

func getCurrentState() -> Node:
	assert (_crtState != null)
	return _crtState

func registerState(stateID: int, stateInstance: Node):
	assert (stateID < _states.size())
	_states[stateID] = stateInstance
