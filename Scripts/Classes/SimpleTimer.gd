# © 2020 Axel L. <ael dot pm at protonmail dot com>

# A single-threaded timer.

class_name SimpleTimer

var _secElapsed = -1.0
var _secLimit: float

func _init(secLimit: float): # Constructor
	_secLimit = secLimit

func getPercentageElapsed() -> float:
	return (_secElapsed / _secLimit)

func getPercentageLeft() -> float:
	return max((_secLimit - _secElapsed) / _secLimit, 0.0)

func hasReachedTimeLimit() -> bool:
	return (_secElapsed >= _secLimit)

func hasStarted() -> bool:
	return (_secElapsed >= 0.0)

func start():
	_secElapsed = 0.0

func stop():
	_secElapsed = -1.0

func update(secDelta: float):
	_secElapsed += secDelta
